/*
SQLyog Ultimate v11.5 (64 bit)
MySQL - 10.1.13-MariaDB : Database - cis
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cis` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `cis`;

/*Table structure for table `adak_kelas` */

DROP TABLE IF EXISTS `adak_kelas`;

CREATE TABLE `adak_kelas` (
  `kelas_id` int(11) NOT NULL AUTO_INCREMENT,
  `ta` int(4) NOT NULL DEFAULT '0',
  `nama` varchar(20) NOT NULL DEFAULT '',
  `ket` text,
  `dosen_wali_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`kelas_id`),
  KEY `FK_adak_kelas_wali` (`dosen_wali_id`),
  CONSTRAINT `FK_adak_kelas_wali` FOREIGN KEY (`dosen_wali_id`) REFERENCES `hrdx_dosen` (`dosen_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `adak_kelas` */

/*Table structure for table `adak_registrasi` */

DROP TABLE IF EXISTS `adak_registrasi`;

CREATE TABLE `adak_registrasi` (
  `registrasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `nim` varchar(8) NOT NULL DEFAULT '',
  `status_akhir_registrasi` varchar(50) DEFAULT 'Aktif',
  `ta` varchar(30) NOT NULL DEFAULT '0',
  `sem_ta` int(11) NOT NULL DEFAULT '0',
  `sem` smallint(6) NOT NULL DEFAULT '0',
  `tgl_daftar` date DEFAULT NULL,
  `keuangan` double DEFAULT NULL,
  `kelas` varchar(20) DEFAULT NULL,
  `id` varchar(20) DEFAULT NULL,
  `nr` float DEFAULT NULL,
  `koa_approval` int(11) NOT NULL DEFAULT '0',
  `koa_approval_bp` int(11) NOT NULL DEFAULT '0',
  `kelas_id` int(11) DEFAULT NULL,
  `dosen_wali_id` int(11) DEFAULT NULL,
  `keasramaan_id` int(11) DEFAULT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`registrasi_id`),
  KEY `fk_t_registrasi_t_kelas1_idx` (`kelas_id`),
  KEY `fk_t_registrasi_t_profile1_idx` (`dosen_wali_id`),
  KEY `fk_t_registrasi_t_dim1_idx` (`dim_id`),
  KEY `keasramaan_id` (`keasramaan_id`),
  CONSTRAINT `FK_adak_registrasi_dosen_wali` FOREIGN KEY (`dosen_wali_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_prkl_registrasi_kelas` FOREIGN KEY (`kelas_id`) REFERENCES `adak_kelas` (`kelas_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `adak_registrasi_ibfk_1` FOREIGN KEY (`keasramaan_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_t_registrasi_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `adak_registrasi` */

/*Table structure for table `auth_assignment` */

DROP TABLE IF EXISTS `auth_assignment`;

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` varchar(64) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `auth_assignment` */

/*Table structure for table `auth_item` */

DROP TABLE IF EXISTS `auth_item`;

CREATE TABLE `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `auth_item` */

/*Table structure for table `auth_item_child` */

DROP TABLE IF EXISTS `auth_item_child`;

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `auth_item_child` */

/*Table structure for table `auth_rule` */

DROP TABLE IF EXISTS `auth_rule`;

CREATE TABLE `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `auth_rule` */

/*Table structure for table `dimx_dim` */

DROP TABLE IF EXISTS `dimx_dim`;

CREATE TABLE `dimx_dim` (
  `dim_id` int(11) NOT NULL AUTO_INCREMENT,
  `nim` varchar(8) NOT NULL DEFAULT '',
  `no_usm` varchar(15) NOT NULL DEFAULT '',
  `jalur` varchar(20) DEFAULT NULL,
  `user_name` varchar(10) DEFAULT NULL,
  `kbk_id` varchar(20) DEFAULT NULL,
  `ref_kbk_id` int(11) DEFAULT NULL,
  `kpt_prodi` varchar(10) DEFAULT NULL,
  `id_kur` int(4) NOT NULL DEFAULT '0',
  `tahun_kurikulum_id` int(11) DEFAULT NULL,
  `nama` varchar(50) NOT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `gol_darah` char(2) DEFAULT NULL,
  `golongan_darah_id` int(11) DEFAULT NULL,
  `jenis_kelamin` char(1) DEFAULT NULL,
  `jenis_kelamin_id` int(11) DEFAULT NULL,
  `agama` varchar(30) DEFAULT NULL,
  `agama_id` int(11) DEFAULT NULL,
  `alamat` text,
  `kabupaten` varchar(50) DEFAULT NULL,
  `kode_pos` varchar(5) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telepon` varchar(50) DEFAULT NULL,
  `hp` varchar(50) DEFAULT NULL,
  `hp2` varchar(50) DEFAULT NULL,
  `no_ijazah_sma` varchar(100) DEFAULT NULL,
  `nama_sma` varchar(50) DEFAULT NULL,
  `asal_sekolah_id` int(11) DEFAULT NULL,
  `alamat_sma` text,
  `kabupaten_sma` varchar(100) DEFAULT NULL,
  `telepon_sma` varchar(50) DEFAULT NULL,
  `kodepos_sma` varchar(8) DEFAULT NULL,
  `thn_masuk` int(11) DEFAULT NULL,
  `status_akhir` varchar(50) DEFAULT 'Aktif',
  `nama_ayah` varchar(50) DEFAULT NULL,
  `nama_ibu` varchar(50) DEFAULT NULL,
  `no_hp_ayah` varchar(50) DEFAULT NULL,
  `no_hp_ibu` varchar(50) DEFAULT NULL,
  `alamat_orangtua` text,
  `pekerjaan_ayah` varchar(100) DEFAULT NULL,
  `pekerjaan_ayah_id` int(11) DEFAULT NULL,
  `keterangan_pekerjaan_ayah` text,
  `penghasilan_ayah` varchar(50) DEFAULT NULL,
  `penghasilan_ayah_id` int(11) DEFAULT NULL,
  `pekerjaan_ibu` varchar(100) DEFAULT NULL,
  `pekerjaan_ibu_id` int(11) DEFAULT NULL,
  `keterangan_pekerjaan_ibu` text,
  `penghasilan_ibu` varchar(50) DEFAULT NULL,
  `penghasilan_ibu_id` int(11) DEFAULT NULL,
  `nama_wali` varchar(50) DEFAULT NULL,
  `pekerjaan_wali` varchar(50) DEFAULT NULL,
  `pekerjaan_wali_id` int(11) DEFAULT NULL,
  `keterangan_pekerjaan_wali` text,
  `penghasilan_wali` varchar(50) DEFAULT NULL,
  `penghasilan_wali_id` int(11) DEFAULT NULL,
  `alamat_wali` text,
  `telepon_wali` varchar(20) DEFAULT NULL,
  `no_hp_wali` varchar(50) DEFAULT NULL,
  `pendapatan` varchar(50) DEFAULT NULL,
  `ipk` float DEFAULT '0',
  `anak_ke` tinyint(4) DEFAULT NULL,
  `dari_jlh_anak` tinyint(4) DEFAULT NULL,
  `jumlah_tanggungan` tinyint(4) DEFAULT NULL,
  `nilai_usm` float DEFAULT NULL,
  `score_iq` tinyint(4) DEFAULT NULL,
  `rekomendasi_psikotest` varchar(4) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `kode_foto` varchar(100) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `akun_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`dim_id`),
  UNIQUE KEY `NIM_UNIQUE` (`nim`),
  KEY `NIM` (`nim`),
  KEY `FK_dimx_dim_thn_krkm` (`tahun_kurikulum_id`),
  KEY `FK_dimx_dim_user` (`user_id`),
  KEY `FK_dimx_dim_ref_kbk` (`ref_kbk_id`),
  KEY `FK_dimx_dim_asal_sekolah` (`asal_sekolah_id`),
  KEY `FK_dimx_dim_golongan_darah` (`golongan_darah_id`),
  KEY `FK_dimx_dim_jenis_kelamin` (`jenis_kelamin_id`),
  KEY `FK_dimx_dim_agama` (`agama_id`),
  KEY `FK_dimx_dim_pekerjaan_ayah` (`pekerjaan_ayah_id`),
  KEY `FK_dimx_dim_penghasilan_ayah` (`penghasilan_ayah_id`),
  KEY `FK_dimx_dim_pekerjaan_ibu` (`pekerjaan_ibu_id`),
  KEY `FK_dimx_dim_penghasilan_ibu` (`penghasilan_ibu_id`),
  KEY `FK_dimx_dim_pekerjaan_wali` (`pekerjaan_wali_id`),
  KEY `FK_dimx_dim_penghasilan_wali_id` (`penghasilan_wali_id`),
  CONSTRAINT `FK_dimx_dim_agama` FOREIGN KEY (`agama_id`) REFERENCES `mref_r_agama` (`agama_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_asal_sekolah` FOREIGN KEY (`asal_sekolah_id`) REFERENCES `mref_r_asal_sekolah` (`asal_sekolah_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_golongan_darah` FOREIGN KEY (`golongan_darah_id`) REFERENCES `mref_r_golongan_darah` (`golongan_darah_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_jenis_kelamin` FOREIGN KEY (`jenis_kelamin_id`) REFERENCES `mref_r_jenis_kelamin` (`jenis_kelamin_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_pekerjaan_ayah` FOREIGN KEY (`pekerjaan_ayah_id`) REFERENCES `mref_r_pekerjaan` (`pekerjaan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_pekerjaan_ibu` FOREIGN KEY (`pekerjaan_ibu_id`) REFERENCES `mref_r_pekerjaan` (`pekerjaan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_pekerjaan_wali` FOREIGN KEY (`pekerjaan_wali_id`) REFERENCES `mref_r_pekerjaan` (`pekerjaan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_penghasilan_ayah` FOREIGN KEY (`penghasilan_ayah_id`) REFERENCES `mref_r_penghasilan` (`penghasilan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_penghasilan_ibu` FOREIGN KEY (`penghasilan_ibu_id`) REFERENCES `mref_r_penghasilan` (`penghasilan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_penghasilan_wali_id` FOREIGN KEY (`penghasilan_wali_id`) REFERENCES `mref_r_penghasilan` (`penghasilan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_ref_kbk` FOREIGN KEY (`ref_kbk_id`) REFERENCES `inst_prodi` (`ref_kbk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_thn_krkm` FOREIGN KEY (`tahun_kurikulum_id`) REFERENCES `krkm_r_tahun_kurikulum` (`tahun_kurikulum_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_user` FOREIGN KEY (`user_id`) REFERENCES `sysx_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `dimx_dim` */

insert  into `dimx_dim`(`dim_id`,`nim`,`no_usm`,`jalur`,`user_name`,`kbk_id`,`ref_kbk_id`,`kpt_prodi`,`id_kur`,`tahun_kurikulum_id`,`nama`,`tgl_lahir`,`tempat_lahir`,`gol_darah`,`golongan_darah_id`,`jenis_kelamin`,`jenis_kelamin_id`,`agama`,`agama_id`,`alamat`,`kabupaten`,`kode_pos`,`email`,`telepon`,`hp`,`hp2`,`no_ijazah_sma`,`nama_sma`,`asal_sekolah_id`,`alamat_sma`,`kabupaten_sma`,`telepon_sma`,`kodepos_sma`,`thn_masuk`,`status_akhir`,`nama_ayah`,`nama_ibu`,`no_hp_ayah`,`no_hp_ibu`,`alamat_orangtua`,`pekerjaan_ayah`,`pekerjaan_ayah_id`,`keterangan_pekerjaan_ayah`,`penghasilan_ayah`,`penghasilan_ayah_id`,`pekerjaan_ibu`,`pekerjaan_ibu_id`,`keterangan_pekerjaan_ibu`,`penghasilan_ibu`,`penghasilan_ibu_id`,`nama_wali`,`pekerjaan_wali`,`pekerjaan_wali_id`,`keterangan_pekerjaan_wali`,`penghasilan_wali`,`penghasilan_wali_id`,`alamat_wali`,`telepon_wali`,`no_hp_wali`,`pendapatan`,`ipk`,`anak_ke`,`dari_jlh_anak`,`jumlah_tanggungan`,`nilai_usm`,`score_iq`,`rekomendasi_psikotest`,`foto`,`kode_foto`,`user_id`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`updated_at`,`created_by`,`updated_by`,`akun_id`) values (12,'21113052','311','USM3','agon',NULL,NULL,NULL,0,NULL,'Rotua','1996-01-17','Haunatas','O',NULL,'P',NULL,'Kristen Protestan',NULL,'Perumahan\r\n','Toba Samosir','22381','rotuapasaribu69@gmail.com',NULL,'085206015693',NULL,'331','SMA 1 Laguboti',NULL,'Partahan Bosi Hutapea','Toba Samosir','0632331551','22381',2010,'Aktif','Ramses','Non','082364453839','082364453839','Perumahan','Wiraswasta',NULL,'Bengkel','300000',NULL,'PNS',NULL,'Guru SD\r\n','2300000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,4,7,7,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'2016-06-01 16:47:53',NULL,NULL,NULL,1),(13,'21113049','113','USM3','komar',NULL,NULL,NULL,0,NULL,'Mariani','1996-07-10','Medan','AB',NULL,'P',NULL,'Kristen Protestan',NULL,'Medan Krakatau','Medan','22315','mariani@gmail.com',NULL,'085712112511',NULL,NULL,'SMA 1 Cahaya Medan',NULL,'Samosir Island','Medan','0632112322','22315',2010,'Aktif','Risman','Rina','0852644533315','085234531234','Krakatau','Wiraswasta',NULL,'Penjual Baju','500000',NULL,'PNS',NULL,'Guru\r\n','1900000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,4,4,1,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'2016-12-14 08:44:21',NULL,NULL,NULL,NULL);

/*Table structure for table `hrdx_dosen` */

DROP TABLE IF EXISTS `hrdx_dosen`;

CREATE TABLE `hrdx_dosen` (
  `dosen_id` int(11) NOT NULL AUTO_INCREMENT,
  `pegawai_id` int(11) DEFAULT NULL,
  `nidn` varchar(10) DEFAULT NULL,
  `prodi_id` int(11) DEFAULT NULL,
  `golongan_kepangkatan_id` int(11) DEFAULT NULL,
  `jabatan_akademik_id` int(11) DEFAULT NULL,
  `status_ikatan_kerja_dosen_id` int(11) DEFAULT NULL,
  `gbk_1` int(11) DEFAULT NULL,
  `gbk_2` int(11) DEFAULT NULL,
  `aktif_start` date DEFAULT '0000-00-00',
  `aktif_end` date DEFAULT '0000-00-00',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `temp_id_old` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`dosen_id`),
  KEY `FK_hrdx_dosen` (`golongan_kepangkatan_id`),
  KEY `FK_hrdx_dosen_jab` (`jabatan_akademik_id`),
  KEY `FK_hrdx_dosen_stts` (`status_ikatan_kerja_dosen_id`),
  KEY `FK_hrdx_dosen_gbk` (`gbk_1`),
  KEY `FK_hrdx_dosen_pegawai` (`pegawai_id`),
  KEY `FK_hrdx_dosen_gbk2` (`gbk_2`),
  KEY `FK_hrdx_dosen_prodi` (`prodi_id`),
  CONSTRAINT `FK_hrdx_dosen` FOREIGN KEY (`golongan_kepangkatan_id`) REFERENCES `mref_r_golongan_kepangkatan` (`golongan_kepangkatan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_dosen_gbk` FOREIGN KEY (`gbk_1`) REFERENCES `mref_r_gbk` (`gbk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_dosen_gbk2` FOREIGN KEY (`gbk_2`) REFERENCES `mref_r_gbk` (`gbk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_dosen_jab` FOREIGN KEY (`jabatan_akademik_id`) REFERENCES `mref_r_jabatan_akademik` (`jabatan_akademik_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_dosen_pegawai` FOREIGN KEY (`pegawai_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_dosen_prodi` FOREIGN KEY (`prodi_id`) REFERENCES `inst_prodi` (`ref_kbk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_dosen_stts` FOREIGN KEY (`status_ikatan_kerja_dosen_id`) REFERENCES `mref_r_status_ikatan_kerja_dosen` (`status_ikatan_kerja_dosen_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `hrdx_dosen` */

insert  into `hrdx_dosen`(`dosen_id`,`pegawai_id`,`nidn`,`prodi_id`,`golongan_kepangkatan_id`,`jabatan_akademik_id`,`status_ikatan_kerja_dosen_id`,`gbk_1`,`gbk_2`,`aktif_start`,`aktif_end`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`,`temp_id_old`) values (3,10,'213131',NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00','0000-00-00',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `hrdx_pegawai` */

DROP TABLE IF EXISTS `hrdx_pegawai`;

CREATE TABLE `hrdx_pegawai` (
  `pegawai_id` int(11) NOT NULL AUTO_INCREMENT,
  `akun_id` int(11) NOT NULL,
  `profile_old_id` varchar(20) DEFAULT NULL,
  `nama` varchar(135) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `nip` varchar(45) DEFAULT NULL,
  `kpt_no` varchar(10) DEFAULT NULL,
  `kbk_id` varchar(20) DEFAULT NULL,
  `ref_kbk_id` int(11) DEFAULT NULL,
  `alias` varchar(9) DEFAULT NULL,
  `posisi` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(60) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `agama_id` int(11) DEFAULT NULL,
  `jenis_kelamin_id` int(11) DEFAULT NULL,
  `golongan_darah_id` int(11) DEFAULT NULL,
  `hp` varchar(20) DEFAULT NULL,
  `telepon` varchar(45) DEFAULT NULL,
  `alamat` blob,
  `alamat_libur` varchar(100) DEFAULT NULL,
  `kecamatan` varchar(150) DEFAULT NULL,
  `kota` varchar(50) DEFAULT NULL,
  `kabupaten_id` int(11) DEFAULT NULL,
  `kode_pos` varchar(15) DEFAULT NULL,
  `no_ktp` varchar(255) DEFAULT NULL,
  `email` text,
  `ext_num` char(3) DEFAULT NULL,
  `study_area_1` varchar(50) DEFAULT NULL,
  `study_area_2` varchar(50) DEFAULT NULL,
  `jabatan` char(1) DEFAULT NULL,
  `jabatan_akademik_id` int(11) DEFAULT NULL,
  `gbk_1` int(11) DEFAULT NULL,
  `gbk_2` int(11) DEFAULT NULL,
  `status_ikatan_kerja_pegawai_id` int(11) DEFAULT NULL,
  `status_akhir` char(1) DEFAULT NULL,
  `status_aktif_pegawai_id` int(11) DEFAULT NULL,
  `tanggal_masuk` date DEFAULT '0000-00-00',
  `tanggal_keluar` date DEFAULT '0000-00-00',
  `nama_bapak` varchar(50) DEFAULT NULL,
  `nama_ibu` varchar(50) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `status_marital_id` int(11) DEFAULT NULL,
  `nama_p` varchar(50) DEFAULT NULL,
  `tgl_lahir_p` date DEFAULT NULL,
  `tmp_lahir_p` varchar(50) DEFAULT NULL,
  `pekerjaan_ortu` varchar(100) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`pegawai_id`),
  KEY `FK_hrdx_pegawai_JK` (`jenis_kelamin_id`),
  KEY `FK_hrdx_pegawai_agama` (`agama_id`),
  KEY `FK_hrdx_pegawai_golda` (`golongan_darah_id`),
  KEY `FK_hrdx_pegawai_kab` (`kabupaten_id`),
  KEY `FK_hrdx_pegawai_sts_aktf` (`status_aktif_pegawai_id`),
  KEY `FK_hrdx_pegawai_sts_iktn` (`status_ikatan_kerja_pegawai_id`),
  KEY `FK_hrdx_pegawai_sts_martl` (`status_marital_id`),
  KEY `FK_hrdx_pegawai_user` (`user_id`),
  KEY `FK_hrdx_pegawai_jabatan_akademik` (`jabatan_akademik_id`),
  KEY `FK_hrdx_pegawai_kbk` (`ref_kbk_id`),
  KEY `akun_id` (`akun_id`),
  CONSTRAINT `FK_hrdx_pegawai_JK` FOREIGN KEY (`jenis_kelamin_id`) REFERENCES `mref_r_jenis_kelamin` (`jenis_kelamin_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_agama` FOREIGN KEY (`agama_id`) REFERENCES `mref_r_agama` (`agama_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_golda` FOREIGN KEY (`golongan_darah_id`) REFERENCES `mref_r_golongan_darah` (`golongan_darah_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_jabatan_akademik` FOREIGN KEY (`jabatan_akademik_id`) REFERENCES `mref_r_jabatan_akademik` (`jabatan_akademik_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_kab` FOREIGN KEY (`kabupaten_id`) REFERENCES `mref_r_kabupaten` (`kabupaten_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_kbk` FOREIGN KEY (`ref_kbk_id`) REFERENCES `inst_prodi` (`ref_kbk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_sts_aktf` FOREIGN KEY (`status_aktif_pegawai_id`) REFERENCES `mref_r_status_aktif_pegawai` (`status_aktif_pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_sts_iktn` FOREIGN KEY (`status_ikatan_kerja_pegawai_id`) REFERENCES `mref_r_status_ikatan_kerja_pegawai` (`status_ikatan_kerja_pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_sts_martl` FOREIGN KEY (`status_marital_id`) REFERENCES `mref_r_status_marital` (`status_marital_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `hrdx_pegawai_ibfk_1` FOREIGN KEY (`akun_id`) REFERENCES `sikk_akun` (`akun_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `hrdx_pegawai` */

insert  into `hrdx_pegawai`(`pegawai_id`,`akun_id`,`profile_old_id`,`nama`,`user_name`,`nip`,`kpt_no`,`kbk_id`,`ref_kbk_id`,`alias`,`posisi`,`tempat_lahir`,`tgl_lahir`,`agama_id`,`jenis_kelamin_id`,`golongan_darah_id`,`hp`,`telepon`,`alamat`,`alamat_libur`,`kecamatan`,`kota`,`kabupaten_id`,`kode_pos`,`no_ktp`,`email`,`ext_num`,`study_area_1`,`study_area_2`,`jabatan`,`jabatan_akademik_id`,`gbk_1`,`gbk_2`,`status_ikatan_kerja_pegawai_id`,`status_akhir`,`status_aktif_pegawai_id`,`tanggal_masuk`,`tanggal_keluar`,`nama_bapak`,`nama_ibu`,`status`,`status_marital_id`,`nama_p`,`tgl_lahir_p`,`tmp_lahir_p`,`pekerjaan_ortu`,`user_id`,`deleted`,`deleted_at`,`deleted_by`,`created_by`,`created_at`,`updated_by`,`updated_at`) values (5,2,NULL,'ria','agon','232322222',NULL,NULL,NULL,'Ria Doden','keasramaan','Sipirok','1988-04-01',NULL,NULL,NULL,'085209091234',NULL,'Del Institute Of Technology',NULL,'Laguboti','Sitoluama',NULL,'22381','1212121111','ria@del.ac.id',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'a',NULL,'2015-07-27','0000-00-00','DOni','Dian',NULL,NULL,NULL,NULL,NULL,'PNS',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(10,5,NULL,'Yohanssen','hansen','2131312331',NULL,NULL,NULL,'Hansen',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00','0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(11,7,NULL,'Satpam Ganteng','satpam','2131312331',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00','0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(12,6,NULL,'Humacak','humasak','2131312331',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00','0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(13,8,NULL,'Yuni','yuni',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00','0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(14,9,NULL,'Elisa','elisa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00','0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `sikk_akun` */

DROP TABLE IF EXISTS `sikk_akun`;

CREATE TABLE `sikk_akun` (
  `akun_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` char(30) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `auth_key` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`akun_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `sikk_akun_ibfk_3` FOREIGN KEY (`role_id`) REFERENCES `sikk_r_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `sikk_akun` */

insert  into `sikk_akun`(`akun_id`,`user_name`,`password`,`role_id`,`deleted`,`created_at`,`created_by`,`updated_at`,`updated_by`,`auth_key`) values (1,'agon','agon',1,NULL,'2016-07-06 16:30:32',NULL,NULL,NULL,NULL),(2,'ria','ria',3,NULL,'2016-03-09 16:53:58',NULL,NULL,NULL,NULL),(4,'rini','rini',3,NULL,'2016-12-14 08:50:50',NULL,NULL,NULL,NULL),(5,'Yohanssen','hansen',4,NULL,'2016-12-14 08:50:50',NULL,NULL,NULL,NULL),(6,'Humasak','humasak',4,NULL,'2016-12-14 08:50:50',NULL,NULL,NULL,NULL),(7,'satpam','satpam',5,NULL,'2016-12-14 08:50:50',NULL,NULL,NULL,NULL),(8,'Yuni','yuni',2,NULL,'2016-12-14 08:50:50',NULL,NULL,NULL,NULL),(9,'Elisa','elisa',2,NULL,'2016-12-14 08:50:50',NULL,NULL,NULL,NULL);

/*Table structure for table `sikk_approval` */

DROP TABLE IF EXISTS `sikk_approval`;

CREATE TABLE `sikk_approval` (
  `approval_id` int(11) NOT NULL AUTO_INCREMENT,
  `izin_keluar_id` int(11) NOT NULL,
  `approver_id` int(11) DEFAULT NULL,
  `status_approval` tinyint(1) DEFAULT NULL,
  `alasan` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`approval_id`),
  KEY `sikk_approval_ibfk_2` (`approver_id`),
  KEY `izin_keluar_id` (`izin_keluar_id`),
  CONSTRAINT `sikk_approval_ibfk_2` FOREIGN KEY (`approver_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sikk_approval_ibfk_3` FOREIGN KEY (`izin_keluar_id`) REFERENCES `sikk_izin_keluar` (`izin_keluar_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `sikk_approval` */

insert  into `sikk_approval`(`approval_id`,`izin_keluar_id`,`approver_id`,`status_approval`,`alasan`,`deleted`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (3,1,10,2,NULL,NULL,'2016-12-14',NULL,NULL,NULL),(4,2,5,3,NULL,NULL,NULL,NULL,NULL,NULL),(5,5,10,2,NULL,NULL,NULL,NULL,NULL,NULL),(6,6,14,1,NULL,NULL,NULL,NULL,NULL,NULL),(7,7,13,2,NULL,NULL,NULL,NULL,NULL,NULL),(8,7,12,0,NULL,NULL,NULL,NULL,NULL,NULL),(9,7,5,0,NULL,NULL,NULL,NULL,NULL,NULL),(10,8,5,3,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `sikk_izin_keluar` */

DROP TABLE IF EXISTS `sikk_izin_keluar`;

CREATE TABLE `sikk_izin_keluar` (
  `izin_keluar_id` int(11) NOT NULL AUTO_INCREMENT,
  `mhs_id` int(11) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `time_start` datetime DEFAULT NULL,
  `time_end` datetime DEFAULT NULL,
  `alasan` varchar(30) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`izin_keluar_id`),
  KEY `mhs_id` (`mhs_id`),
  CONSTRAINT `sikk_izin_keluar_ibfk_1` FOREIGN KEY (`mhs_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `sikk_izin_keluar` */

insert  into `sikk_izin_keluar`(`izin_keluar_id`,`mhs_id`,`date_start`,`date_end`,`time_start`,`time_end`,`alasan`,`deleted`,`created_at`,`created_by`,`updated_at`,`updated_by`,`status`) values (1,12,'2016-11-08','2016-11-08','2016-11-08 13:50:21','2016-11-08 16:50:34',NULL,NULL,'2016-12-06 16:50:43',NULL,NULL,NULL,33),(2,13,'2016-11-01','2016-11-01','2016-11-01 08:00:36','2016-11-01 12:00:50',NULL,NULL,'2016-12-14 08:49:04',NULL,NULL,NULL,3),(3,12,'2016-12-28',NULL,'2016-12-28 03:17:45','2016-12-28 05:00:00','Burjer',NULL,'2016-12-15 02:58:55','agon',NULL,NULL,NULL),(4,12,'2016-12-15','2016-12-20','2016-12-15 03:17:45','2016-12-20 05:00:00','Ke dokter',NULL,'2016-12-15 03:41:39','agon',NULL,NULL,NULL),(5,12,'2016-12-13','2016-12-16','2016-12-13 03:17:45','2016-12-16 05:00:00','foto foto',NULL,'2016-12-15 04:27:31','agon',NULL,NULL,3),(6,12,'2016-12-12','2016-12-23','2016-12-12 03:17:45','2016-12-23 05:00:00','Sakit ',NULL,'2016-12-15 04:34:48','agon',NULL,NULL,NULL),(7,12,'2016-12-15','2016-12-20','2016-12-15 03:17:45','2016-12-20 05:00:00','Pernikahan Abang',NULL,'2016-12-15 03:41:39','agon',NULL,NULL,NULL),(8,12,'2016-12-12','2016-12-15','2016-12-12 03:17:45','2016-12-15 05:00:00','Ijin Ngurus STNK',NULL,'2016-12-15 05:32:27','agon',NULL,NULL,33);

/*Table structure for table `sikk_log` */

DROP TABLE IF EXISTS `sikk_log`;

CREATE TABLE `sikk_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `mhs_id` int(11) DEFAULT NULL,
  `time_in` datetime DEFAULT NULL,
  `time_out` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `mhs_id` (`mhs_id`),
  CONSTRAINT `sikk_log_ibfk_1` FOREIGN KEY (`mhs_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `sikk_log` */

insert  into `sikk_log`(`log_id`,`mhs_id`,`time_in`,`time_out`,`deleted`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,12,'2016-12-07 16:00:06','2016-12-07 13:49:39',NULL,'2016-12-01',NULL,NULL,NULL),(2,13,'2016-12-15 03:47:28',NULL,NULL,'2016-12-15',NULL,NULL,NULL),(3,12,'2016-12-15 03:47:35',NULL,NULL,'2016-12-15',NULL,NULL,NULL),(4,13,'2016-12-15 04:03:36',NULL,NULL,'2016-12-15',NULL,NULL,NULL),(5,13,'2016-12-15 04:20:26',NULL,NULL,'2016-12-15',NULL,NULL,NULL),(6,13,'2016-12-15 05:40:29',NULL,NULL,'2016-12-15',NULL,NULL,NULL);

/*Table structure for table `sikk_r_role` */

DROP TABLE IF EXISTS `sikk_r_role`;

CREATE TABLE `sikk_r_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(32) DEFAULT NULL,
  `desc` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `sikk_r_role` */

insert  into `sikk_r_role`(`role_id`,`role_name`,`desc`,`deleted`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,'mahasiswa','yang melakukan request keluar kampus pada jam akad',NULL,NULL,NULL,NULL,NULL),(2,'dosen','memberikan approval terhadap request data yang dil',NULL,NULL,NULL,NULL,NULL),(3,'keasramaan','memberikan approval terhadap request data yang dil',NULL,NULL,NULL,NULL,NULL),(4,'dosen_wali','yang memberikan persetujuan awal atas permintaan i',NULL,NULL,NULL,NULL,NULL),(5,'satpam','memberikan persetujuan bahwa mahasiswa dapat izin ',NULL,NULL,NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
