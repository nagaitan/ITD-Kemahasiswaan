<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sikk_log".
 *
 * @property integer $log_id
 * @property integer $mhs_id
 * @property string $time_in
 * @property string $time_out
 * @property integer $deleted
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property DimxDim $mhs
 */
class SikkLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sikk_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mhs_id', 'deleted'], 'integer'],
            [['time_in', 'time_out', 'created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'string', 'max' => 32],
            [['mhs_id'], 'exist', 'skipOnError' => true, 'targetClass' => DimxDim::className(), 'targetAttribute' => ['mhs_id' => 'dim_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'log_id' => 'Log ID',
            'mhs_id' => 'Mhs ID',
            'time_in' => 'Time In',
            'time_out' => 'Time Out',
            'deleted' => 'Deleted',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMhs()
    {
        return $this->hasOne(DimxDim::className(), ['dim_id' => 'mhs_id']);
    }
}
