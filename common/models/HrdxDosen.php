<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hrdx_dosen".
 *
 * @property integer $dosen_id
 * @property integer $pegawai_id
 * @property string $nidn
 * @property integer $prodi_id
 * @property integer $golongan_kepangkatan_id
 * @property integer $jabatan_akademik_id
 * @property integer $status_ikatan_kerja_dosen_id
 * @property integer $gbk_1
 * @property integer $gbk_2
 * @property string $aktif_start
 * @property string $aktif_end
 * @property integer $deleted
 * @property string $deleted_at
 * @property string $deleted_by
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property string $temp_id_old
 *
 * @property AdakKelas[] $adakKelas
 * @property HrdxPegawai $pegawai
 */
class HrdxDosen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hrdx_dosen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pegawai_id', 'prodi_id', 'golongan_kepangkatan_id', 'jabatan_akademik_id', 'status_ikatan_kerja_dosen_id', 'gbk_1', 'gbk_2', 'deleted'], 'integer'],
            [['aktif_start', 'aktif_end', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['nidn'], 'string', 'max' => 10],
            [['deleted_by', 'created_by', 'updated_by'], 'string', 'max' => 32],
            [['temp_id_old'], 'string', 'max' => 100],
            [['pegawai_id'], 'exist', 'skipOnError' => true, 'targetClass' => HrdxPegawai::className(), 'targetAttribute' => ['pegawai_id' => 'pegawai_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dosen_id' => 'Dosen ID',
            'pegawai_id' => 'Pegawai ID',
            'nidn' => 'Nidn',
            'prodi_id' => 'Prodi ID',
            'golongan_kepangkatan_id' => 'Golongan Kepangkatan ID',
            'jabatan_akademik_id' => 'Jabatan Akademik ID',
            'status_ikatan_kerja_dosen_id' => 'Status Ikatan Kerja Dosen ID',
            'gbk_1' => 'Gbk 1',
            'gbk_2' => 'Gbk 2',
            'aktif_start' => 'Aktif Start',
            'aktif_end' => 'Aktif End',
            'deleted' => 'Deleted',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'temp_id_old' => 'Temp Id Old',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdakKelas()
    {
        return $this->hasMany(AdakKelas::className(), ['dosen_wali_id' => 'dosen_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai()
    {
        return $this->hasOne(HrdxPegawai::className(), ['pegawai_id' => 'pegawai_id']);
    }
}
