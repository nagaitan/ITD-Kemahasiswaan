<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\HrdxDosen as HrdxDosenModel;

/**
 * HrdxDosen represents the model behind the search form about `common\models\HrdxDosen`.
 */
class HrdxDosen extends HrdxDosenModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dosen_id', 'pegawai_id', 'prodi_id', 'golongan_kepangkatan_id', 'jabatan_akademik_id', 'status_ikatan_kerja_dosen_id', 'gbk_1', 'gbk_2', 'deleted'], 'integer'],
            [['nidn', 'aktif_start', 'aktif_end', 'deleted_at', 'deleted_by', 'created_at', 'created_by', 'updated_at', 'updated_by', 'temp_id_old'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HrdxDosenModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'dosen_id' => $this->dosen_id,
            'pegawai_id' => $this->pegawai_id,
            'prodi_id' => $this->prodi_id,
            'golongan_kepangkatan_id' => $this->golongan_kepangkatan_id,
            'jabatan_akademik_id' => $this->jabatan_akademik_id,
            'status_ikatan_kerja_dosen_id' => $this->status_ikatan_kerja_dosen_id,
            'gbk_1' => $this->gbk_1,
            'gbk_2' => $this->gbk_2,
            'aktif_start' => $this->aktif_start,
            'aktif_end' => $this->aktif_end,
            'deleted' => $this->deleted,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'nidn', $this->nidn])
            ->andFilterWhere(['like', 'deleted_by', $this->deleted_by])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by])
            ->andFilterWhere(['like', 'temp_id_old', $this->temp_id_old]);

        return $dataProvider;
    }
}
