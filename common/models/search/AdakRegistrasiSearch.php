<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AdakRegistrasi;

/**
 * AdakRegistrasiSearch represents the model behind the search form about `common\models\AdakRegistrasi`.
 */
class AdakRegistrasiSearch extends AdakRegistrasi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['registrasi_id', 'sem_ta', 'sem', 'koa_approval', 'koa_approval_bp', 'kelas_id', 'dosen_wali_id', 'keasramaan_id', 'dim_id', 'deleted'], 'integer'],
            [['nim', 'status_akhir_registrasi', 'ta', 'tgl_daftar', 'kelas', 'id', 'deleted_at', 'deleted_by', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'safe'],
            [['keuangan', 'nr'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AdakRegistrasi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'registrasi_id' => $this->registrasi_id,
            'sem_ta' => $this->sem_ta,
            'sem' => $this->sem,
            'tgl_daftar' => $this->tgl_daftar,
            'keuangan' => $this->keuangan,
            'nr' => $this->nr,
            'koa_approval' => $this->koa_approval,
            'koa_approval_bp' => $this->koa_approval_bp,
            'kelas_id' => $this->kelas_id,
            'dosen_wali_id' => $this->dosen_wali_id,
            'keasramaan_id' => $this->keasramaan_id,
            'dim_id' => $this->dim_id,
            'deleted' => $this->deleted,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'nim', $this->nim])
            ->andFilterWhere(['like', 'status_akhir_registrasi', $this->status_akhir_registrasi])
            ->andFilterWhere(['like', 'ta', $this->ta])
            ->andFilterWhere(['like', 'kelas', $this->kelas])
            ->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'deleted_by', $this->deleted_by])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }
}
