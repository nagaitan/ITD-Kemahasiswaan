<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sikk_r_role".
 *
 * @property integer $role_id
 * @property string $role_name
 * @property string $desc
 * @property integer $deleted
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property SikkAkun[] $sikkAkuns
 */
class SikkRRole extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sikk_r_role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['deleted'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['role_name', 'created_by', 'updated_by'], 'string', 'max' => 32],
            [['desc'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'role_id' => 'Role ID',
            'role_name' => 'Role Name',
            'desc' => 'Desc',
            'deleted' => 'Deleted',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSikkAkuns()
    {
        return $this->hasMany(SikkAkun::className(), ['role_id' => 'role_id']);
    }
}
