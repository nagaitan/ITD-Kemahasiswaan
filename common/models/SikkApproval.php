<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sikk_approval".
 *
 * @property integer $approval_id
 * @property integer $izin_keluar_id
 * @property integer $approver_id
 * @property integer $status_approval
 * @property string $alasan
 * @property integer $deleted
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property HrdxPegawai $approver
 * @property SikkIzinKeluar $izinKeluar
 */
class SikkApproval extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sikk_approval';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['izin_keluar_id'], 'required'],
            [['izin_keluar_id', 'approver_id', 'status_approval', 'deleted'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['alasan'], 'string', 'max' => 50],
            [['created_by', 'updated_by'], 'string', 'max' => 32],
            [['approver_id'], 'exist', 'skipOnError' => true, 'targetClass' => HrdxPegawai::className(), 'targetAttribute' => ['approver_id' => 'pegawai_id']],
            [['izin_keluar_id'], 'exist', 'skipOnError' => true, 'targetClass' => SikkIzinKeluar::className(), 'targetAttribute' => ['izin_keluar_id' => 'izin_keluar_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'approval_id' => 'Approval ID',
            'izin_keluar_id' => 'Izin Keluar ID',
            'approver_id' => 'Approver ID',
            'status_approval' => 'Status Approval',
            'alasan' => 'Alasan',
            'deleted' => 'Deleted',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprover()
    {
        return $this->hasOne(HrdxPegawai::className(), ['pegawai_id' => 'approver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIzinKeluar()
    {
        return $this->hasOne(SikkIzinKeluar::className(), ['izin_keluar_id' => 'izin_keluar_id']);
    }
}
