<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sikk_izin_keluar".
 *
 * @property integer $izin_keluar_id
 * @property integer $mhs_id
 * @property string $date_start
 * @property string $date_end
 * @property string $time_start
 * @property string $time_end
 * @property string $alasan
 * @property integer $deleted
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property integer $status
 *
 * @property SikkApproval[] $sikkApprovals
 * @property DimxDim $mhs
 */
class SikkIzinKeluar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sikk_izin_keluar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mhs_id', 'deleted', 'status'], 'integer'],
            [['date_start', 'date_end', 'time_start', 'time_end', 'created_at', 'updated_at'], 'safe'],
            [['alasan'], 'string', 'max' => 30],
            [['created_by', 'updated_by'], 'string', 'max' => 32],
            [['mhs_id'], 'exist', 'skipOnError' => true, 'targetClass' => DimxDim::className(), 'targetAttribute' => ['mhs_id' => 'dim_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'izin_keluar_id' => 'Izin Keluar ID',
            'mhs_id' => 'Mhs ID',
            'date_start' => 'Date Start',
            'date_end' => 'Date End',
            'time_start' => 'Time Start',
            'time_end' => 'Time End',
            'alasan' => 'Alasan',
            'deleted' => 'Deleted',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSikkApprovals()
    {
        return $this->hasMany(SikkApproval::className(), ['izin_keluar_id' => 'izin_keluar_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMhs()
    {
        return $this->hasOne(DimxDim::className(), ['dim_id' => 'mhs_id']);
    }
}
