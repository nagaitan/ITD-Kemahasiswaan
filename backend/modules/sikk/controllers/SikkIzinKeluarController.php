<?php

namespace backend\modules\sikk\controllers;

use Yii;
use common\models\SikkIzinKeluar;
use common\models\search\SikkIzinKeluarSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SikkIzinKeluarController implements the CRUD actions for SikkIzinKeluar model.
 */
class SikkIzinKeluarController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SikkIzinKeluar models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new \common\models\search\SikkApprovalSearch();

        $mus = Yii::$app->user->identity->id;

        $akunID = \common\models\HrdxPegawai::findOne(array('akun_id' => $mus));

        $muso = \common\models\SikkAkun::findOne(array('akun_id' => $mus));

        $roleID = $muso->role_id;


        if ($roleID == 2) {
            //dosen
//            die();
            $roleks = 0;
            $query = \common\models\SikkIzinKeluar::findBySql("SELECT b.izin_keluar_id, b.mhs_id, b.date_start, b.date_end, b.time_start, b.time_end, b.alasan, b.deleted ,b.created_at, b.created_by, b.updated_at, b.updated_by, b.status
 FROM sikk_approval a LEFT JOIN sikk_izin_keluar b ON a.`izin_keluar_id` = b.`izin_keluar_id` WHERE a.`approver_id` = " . $akunID->pegawai_id . ' and b.status=' . $roleks);
        } else if ($roleID == 3) {
            //keasramaan
            $roleks = 2;
            $query = \common\models\SikkIzinKeluar::findBySql("SELECT b.izin_keluar_id, b.mhs_id, b.date_start, b.date_end, b.time_start, b.time_end, b.alasan, b.deleted ,b.created_at, b.created_by, b.updated_at, b.updated_by, b.status
 FROM sikk_approval a LEFT JOIN sikk_izin_keluar b ON a.`izin_keluar_id` = b.`izin_keluar_id` WHERE a.`approver_id` = " . $akunID->pegawai_id . ' and b.status=' . $roleks);
        } else if ($roleID == 4) {
            //dosen wali

            $roleks = 1;
            $query = \common\models\SikkIzinKeluar::findBySql("SELECT b.izin_keluar_id, b.mhs_id, b.date_start, b.date_end, b.time_start, b.time_end, b.alasan, b.deleted ,b.created_at, b.created_by, b.updated_at, b.updated_by, b.status
 FROM sikk_approval a LEFT JOIN sikk_izin_keluar b ON a.`izin_keluar_id` = b.`izin_keluar_id` WHERE a.`approver_id` = " . $akunID->pegawai_id . ' and b.status=' . $roleks);
        } else if ($roleID == 5) {
            //satpam
        }

        $dataProvider = new \yii\data\ActiveDataProvider(
                ["query" => $query]);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndex2() {

        $searchModel = new SikkIzinKeluarSearch();
        $dataProvider = $searchModel->searchs(Yii::$app->request->queryParams);

        return $this->render('index_1', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndex3() {

        $searchModel = new SikkIzinKeluarSearch();
        $dataProvider = $searchModel->searchss(Yii::$app->request->queryParams);

        return $this->render('index_2', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SikkIzinKeluar model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionAcceptwali($id) {

        $model = $this->findModel($id);

        $mus = Yii::$app->user->identity->id;

        $akunID = \common\models\HrdxPegawai::findOne(array('akun_id' => $mus));

        $mdlTL = \common\models\SikkApproval::findOne(array('izin_keluar_id' => $id, 'approver_id' => $akunID->pegawai_id));

        if ($mdlTL) {
            $mdlTL->status_approval = 2;
            if ($mdlTL->save()) {
                $model->status = 2;
                $model->save();
            }

            return $this->redirect(['index']);
        } else {

            return $this->render('createacceptresponsewali', [
                        'mdlTL' => $mdlTL
            ]);
        }
    }

    public function actionRejectwali($id) {

        $model = $this->findModel($id);
        $mdlTL = \common\models\SikkApproval::findOne(array('izin_keluar_id' => $id));
        $mus = Yii::$app->user->identity->id;

        $akunID = \common\models\HrdxPegawai::findOne(array('akun_id' => $mus));

        $mdlTL = \common\models\SikkApproval::findOne(array('izin_keluar_id' => $id, 'approver_id' => $akunID->pegawai_id));

        if ($mdlTL->load(Yii::$app->request->post())) {
            $mdlTL->status_approval = 22;
            if ($mdlTL->save()) {
                $model->status = 22;
                $model->save();
            }

            return $this->redirect(['index']);
        } else {

            return $this->render('createrejectresponsewali', [
                        'mdlTL' => $mdlTL
            ]);
        }
    }

    public function actionAcceptdp($id) {

        $model = $this->findModel($id);
        $mdlTL = \common\models\SikkApproval::findOne(array('izin_keluar_id' => $id));
        $mus = Yii::$app->user->identity->id;

        $akunID = \common\models\HrdxPegawai::findOne(array('akun_id' => $mus));

        $mdlTL = \common\models\SikkApproval::findOne(array('izin_keluar_id' => $id, 'approver_id' => $akunID->pegawai_id));

        if ($mdlTL) {
            $mdlTL->status_approval = 1;
            if ($mdlTL->save()) {
                $model->status = 1;
                $model->save();
            }

            return $this->redirect(['index']);
        } else {

            return $this->render('createacceptresponsewali', [
                        'mdlTL' => $mdlTL
            ]);
        }
    }

    public function actionRejectdp($id) {

        $model = $this->findModel($id);
        $mdlTL = \common\models\SikkApproval::findOne(array('izin_keluar_id' => $id));
        $mus = Yii::$app->user->identity->id;

        $akunID = \common\models\HrdxPegawai::findOne(array('akun_id' => $mus));

        $mdlTL = \common\models\SikkApproval::findOne(array('izin_keluar_id' => $id, 'approver_id' => $akunID->pegawai_id));

        if ($mdlTL->load(Yii::$app->request->post())) {
            $mdlTL->status_approval = 11;
            if ($mdlTL->save()) {
                $model->status = 11;
                $model->save();
            }

            return $this->redirect(['index']);
        } else {

            return $this->render('createrejectresponsewali', [
                        'mdlTL' => $mdlTL
            ]);
        }
    }

    public function actionAcceptkeasramaan($id) {

        $model = $this->findModel($id);
        $mdlTL = \common\models\SikkApproval::findOne(array('izin_keluar_id' => $id));
        $mus = Yii::$app->user->identity->id;

        $akunID = \common\models\HrdxPegawai::findOne(array('akun_id' => $mus));

        $mdlTL = \common\models\SikkApproval::findOne(array('izin_keluar_id' => $id, 'approver_id' => $akunID->pegawai_id));

        if ($mdlTL) {
            $mdlTL->status_approval = 3;
            if ($mdlTL->save()) {
                $model->status = 3;
                $model->save();
            }

            return $this->redirect(['index']);
        } else {

            return $this->render('createacceptresponsewali', [
                        'mdlTL' => $mdlTL
            ]);
        }
    }

    public function actionRejectkeasramaan($id) {

        $model = $this->findModel($id);
        $mdlTL = \common\models\SikkApproval::findOne(array('izin_keluar_id' => $id));
        $mus = Yii::$app->user->identity->id;

        $akunID = \common\models\HrdxPegawai::findOne(array('akun_id' => $mus));

        $mdlTL = \common\models\SikkApproval::findOne(array('izin_keluar_id' => $id, 'approver_id' => $akunID->pegawai_id));

        if ($mdlTL->load(Yii::$app->request->post())) {
            $mdlTL->status_approval = 33;
            if ($mdlTL->save()) {
                $model->status = 33;
                $model->save();
            }

            return $this->redirect(['index']);
        } else {

            return $this->render('createrejectresponsewali', [
                        'mdlTL' => $mdlTL
            ]);
        }
    }

    /**
     * Creates a new SikkIzinKeluar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new SikkIzinKeluar();
        $id_user = Yii::$app->user->identity->akun_id;
        $rows = (new \yii\db\Query())
                ->select(['dim_id'])
                ->from('dimx_dim')
                ->where(['akun_id' => $id_user])
                ->limit(1)
                ->all();
        $dim = $rows[0]["dim_id"];

        if ($model->load(Yii::$app->request->post())) {
            $model->mhs_id = $dim;
            $model->created_at = date('Y-m-d H:i:s');
            $model->created_by = Yii::$app->user->identity->user_name;
            $hour_start = $model->time_start;
            $model->time_start = $model->date_start . " " . $hour_start;
            $hour_end = $model->time_end;
            if ($model->date_end != "") {
                $model->time_end = $model->date_end . " " . $hour_end;
            } else {
                $model->time_end = $model->date_start . " " . $hour_end;
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->izin_keluar_id]);
            } else {
                return $this->render('create', [
                            'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SikkIzinKeluar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->izin_keluar_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SikkIzinKeluar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SikkIzinKeluar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SikkIzinKeluar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = SikkIzinKeluar::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}