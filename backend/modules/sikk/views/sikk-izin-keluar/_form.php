<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\SikkIzinKeluar */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
div.ui-datepicker{
 font-size:20px;
}

input.hasDatepicker{
    width: 99.7%;
    height: 33px;
}
</style>

<div class="sikk-izin-keluar-form">
    <br>
    <?php $form = ActiveForm::begin(); ?>

    <?php //= $form->field($model, 'mhs_id')->textInput() ?>
     <table>
        <tr>
            <td style="vertical-align:top">Mohon izin untuk :</td>
            <td><input type="radio" name="radioName" value="0" onchange="handleChange1();"/> Tidak hadir kuliah/praktikum <br/> 
                <input type="radio" name="radioName" value="1" onchange="handleChange2();"/> Keluar kampus pada jam akademik
            </td>
        </tr>
        <tr>
            <td id="periode" style="vertical-align:top; display: none">Periode :</td>
            <td id="chck1" style="display: none"><input type="radio" name="periode" value="0" onchange="handleChange3();"/> Pada periode jam akademik <br/> 
                <input type="radio" name="periode" value="1" onchange="handleChange4();"/> Selama satu hari lebih
            </td>
        </tr>
    </table>
    <br/><br/>
    <table width='100%'>
        <tr>
            <td>
    <?php //= $form->field($model, 'date_start')->textInput() 
    echo "<b>Hari/Tanggal</b> <br>";

        echo DatePicker::widget([
        'model' => $model,
        'attribute' => 'date_start',
    //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
]);
    ?>
            </td>
            <td id="end_date">
    <?php //= $form->field($model, 'date_end')->textInput() 
    echo "<b>Hingga</b> <br>";
        echo DatePicker::widget([
        'model' => $model,
        'attribute' => 'date_end',
    //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
]);
    ?>
            
            </td>
            <td>
        <tr>
            <td><br/></td>
            <td><br/></td>
        </tr>

        <tr>
            <td>
    <?= $form->field($model, 'time_start')->textInput() ?>
            </td>
            <td>
    <?= $form->field($model, 'time_end')->textInput() ?>
            </td>
        </tr>
        <tr>
            <td colspan='2'>
    <?= $form->field($model, 'alasan')->textArea() ?>
            </td>
        </tr>
    </table>
    <?php //= $form->field($model, 'deleted')->textInput() ?>

    <?php //= $form->field($model, 'created_at')->textInput() ?>

    <?php //= $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?>

    <?php //= $form->field($model, 'updated_at')->textInput() ?>

    <?php //= $form->field($model, 'updated_by')->textInput(['maxlength' => true]) ?>

    <?php //= $form->field($model, 'status')->textInput() ?>
    



    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    function handleChange1() {
    $( "#periode" ).toggle();
    $( "#chck1" ).toggle();
    }
    function handleChange2() {
    $( "#periode" ).hide();
    $( "#chck1" ).hide();
    }
    function handleChange3() {
    $( "#end_date" ).hide();
    }
    function handleChange4() {
    $( "#end_date" ).toggle();
    }
</script>