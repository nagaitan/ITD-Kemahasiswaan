<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SikkIzinKeluar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sikk-izin-keluar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($mdlTL, 'alasan')->textarea(['rows' => 6]) ?>


    <div class="form-group">
        <?= Html::submitButton($mdlTL->isNewRecord ? 'Create' : 'Updates', ['class' => $mdlTL->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
