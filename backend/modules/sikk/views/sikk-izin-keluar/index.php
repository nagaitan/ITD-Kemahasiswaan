<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SikkIzinKeluarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sikk Izin Keluars';
$this->params['breadcrumbs'][] = $this->title;

$mus = Yii::$app->user->identity->id;
$akunID = \common\models\SikkAkun::findOne(array('akun_id' => $mus));
$roleID = $akunID->role_id;
?>
<div class="sikk-izin-keluar-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php
// echo $this->render('_search', ['model' => $searchModel]); 
    if ($roleID == 1) {
        ?>

        <p>
            <?= Html::a('Create Sikk Izin Keluar', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                'izin_keluar_id',
                'mhs.nama',
                'date_start',
                'date_end',
                    ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>

        <?php
    } else {
        ?>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                'izin_keluar_id',
                'mhs.nama',
                'date_start',
                'date_end',
                    ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>

        <?php
    }
    ?>
</div>
