<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SikkIzinKeluar */

$this->title = $model->izin_keluar_id;
$this->params['breadcrumbs'][] = ['label' => 'Sikk Izin Keluars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$mus = Yii::$app->user->identity->id;
$akunID = \common\models\SikkAkun::findOne(array('akun_id' => $mus));
$roleID = $akunID->role_id;

//var_dump($roleID);
//die();
?>
<div class="sikk-izin-keluar-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
        if ($roleID == 4) {
            ?> 

            <?= Html::a('Accept', ['acceptwali', 'id' => $model->izin_keluar_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Reject', ['rejectwali', 'id' => $model->izin_keluar_id], ['class' => 'btn btn-primary']) ?>

            <?=
            Html::a('Delete', ['delete', 'id' => $model->izin_keluar_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
            <?php
        } else if ($roleID == 3) {
            ?>
            <?= Html::a('Accept', ['acceptkeasramaan', 'id' => $model->izin_keluar_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Reject', ['rejectkeasramaan', 'id' => $model->izin_keluar_id], ['class' => 'btn btn-primary']) ?>

            <?=
            Html::a('Delete', ['delete', 'id' => $model->izin_keluar_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>

            <?php
        }else if ($roleID == 2) {
            ?>
            <?= Html::a('Accept', ['acceptdp', 'id' => $model->izin_keluar_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Reject', ['rejectdp', 'id' => $model->izin_keluar_id], ['class' => 'btn btn-primary']) ?>

            <?=
            Html::a('Delete', ['delete', 'id' => $model->izin_keluar_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>

            <?php
        }
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'izin_keluar_id',
            'mhs_id',
            'date_start',
            'date_end',
            'time_start',
            'time_end',
            'alasan',
            'deleted',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
            'status',
        ],
    ])
    ?>

</div>
