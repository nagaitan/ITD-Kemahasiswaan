    <?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SikkIzinKeluar */

$this->title = 'Create Sikk Izin Keluar';
$this->params['breadcrumbs'][] = ['label' => 'Sikk Izin Keluars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sikk-izin-keluar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
