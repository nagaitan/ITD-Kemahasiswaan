<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SikkAkun */

$this->title = $model->akun_id;
$this->params['breadcrumbs'][] = ['label' => 'Sikk Akuns', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sikk-akun-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->akun_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->akun_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'akun_id',
            'user_name',
            'password',
            'role_id',
            'user_id',
            'deleted',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
            'auth_key',
        ],
    ]) ?>

</div>
