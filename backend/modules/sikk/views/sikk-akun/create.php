<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SikkAkun */

$this->title = 'Create Sikk Akun';
$this->params['breadcrumbs'][] = ['label' => 'Sikk Akuns', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sikk-akun-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
