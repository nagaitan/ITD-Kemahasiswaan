<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AdakKelas */

$this->title = 'Create Adak Kelas';
$this->params['breadcrumbs'][] = ['label' => 'Adak Kelas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adak-kelas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
