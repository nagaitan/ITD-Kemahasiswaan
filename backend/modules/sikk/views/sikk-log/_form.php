<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SikkLog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sikk-log-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'nim')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
