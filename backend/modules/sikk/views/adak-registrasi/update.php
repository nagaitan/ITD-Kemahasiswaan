<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AdakRegistrasi */

$this->title = 'Update Adak Registrasi: ' . $model->registrasi_id;
$this->params['breadcrumbs'][] = ['label' => 'Adak Registrasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->registrasi_id, 'url' => ['view', 'id' => $model->registrasi_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="adak-registrasi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
