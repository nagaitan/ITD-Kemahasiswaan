<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\AdakRegistrasi */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Adak Registrasis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adak-registrasi-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Adak Registrasi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'registrasi_id',
            'nim',
            'status_akhir_registrasi',
            'ta',
            'sem_ta',
            // 'sem',
            // 'tgl_daftar',
            // 'keuangan',
            // 'kelas',
            // 'id',
            // 'nr',
            // 'koa_approval',
            // 'koa_approval_bp',
            // 'kelas_id',
            // 'dosen_wali_id',
            // 'keasramaan_id',
            // 'dim_id',
            // 'deleted',
            // 'deleted_at',
            // 'deleted_by',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
