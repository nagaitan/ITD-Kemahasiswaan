<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\AdakRegistrasi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adak-registrasi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'registrasi_id') ?>

    <?= $form->field($model, 'nim') ?>

    <?= $form->field($model, 'status_akhir_registrasi') ?>

    <?= $form->field($model, 'ta') ?>

    <?= $form->field($model, 'sem_ta') ?>

    <?php // echo $form->field($model, 'sem') ?>

    <?php // echo $form->field($model, 'tgl_daftar') ?>

    <?php // echo $form->field($model, 'keuangan') ?>

    <?php // echo $form->field($model, 'kelas') ?>

    <?php // echo $form->field($model, 'id') ?>

    <?php // echo $form->field($model, 'nr') ?>

    <?php // echo $form->field($model, 'koa_approval') ?>

    <?php // echo $form->field($model, 'koa_approval_bp') ?>

    <?php // echo $form->field($model, 'kelas_id') ?>

    <?php // echo $form->field($model, 'dosen_wali_id') ?>

    <?php // echo $form->field($model, 'keasramaan_id') ?>

    <?php // echo $form->field($model, 'dim_id') ?>

    <?php // echo $form->field($model, 'deleted') ?>

    <?php // echo $form->field($model, 'deleted_at') ?>

    <?php // echo $form->field($model, 'deleted_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
