<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AdakRegistrasi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adak-registrasi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nim')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status_akhir_registrasi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ta')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sem_ta')->textInput() ?>

    <?= $form->field($model, 'sem')->textInput() ?>

    <?= $form->field($model, 'tgl_daftar')->textInput() ?>

    <?= $form->field($model, 'keuangan')->textInput() ?>

    <?= $form->field($model, 'kelas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nr')->textInput() ?>

    <?= $form->field($model, 'koa_approval')->textInput() ?>

    <?= $form->field($model, 'koa_approval_bp')->textInput() ?>

    <?= $form->field($model, 'kelas_id')->textInput() ?>

    <?= $form->field($model, 'dosen_wali_id')->textInput() ?>

    <?= $form->field($model, 'keasramaan_id')->textInput() ?>

    <?= $form->field($model, 'dim_id')->textInput() ?>

    <?= $form->field($model, 'deleted')->textInput() ?>

    <?= $form->field($model, 'deleted_at')->textInput() ?>

    <?= $form->field($model, 'deleted_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_by')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
