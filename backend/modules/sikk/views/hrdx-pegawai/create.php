<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HrdxPegawai */

$this->title = 'Create Hrdx Pegawai';
$this->params['breadcrumbs'][] = ['label' => 'Hrdx Pegawais', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hrdx-pegawai-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
