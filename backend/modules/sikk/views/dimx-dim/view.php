<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\DimxDim */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Dimx Dims', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dimx-dim-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-12">

            <div class="  box-header with-border">                
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-2">
                    <?= Html::img('' . \Yii::$app->request->BaseUrl . '/images/' . $model->nama . '.png', ['width' => 130, 'height' => 180]); ?>
                    <?php //die(Yii::$app->basePath);  ?>
                </div>
                <div class="col-md-10">
                    <?=
                    DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            
                            'nama',
                            'nim',
                        //'foto',
                        // 'makanDiluar',
                        ],
                    ])
                    ?>
                </div>
            </div></div></div>

</div>
