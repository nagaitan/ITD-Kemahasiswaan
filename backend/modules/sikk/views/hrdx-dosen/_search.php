<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\HrdxDosen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hrdx-dosen-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'dosen_id') ?>

    <?= $form->field($model, 'pegawai_id') ?>

    <?= $form->field($model, 'nidn') ?>

    <?= $form->field($model, 'prodi_id') ?>

    <?= $form->field($model, 'golongan_kepangkatan_id') ?>

    <?php // echo $form->field($model, 'jabatan_akademik_id') ?>

    <?php // echo $form->field($model, 'status_ikatan_kerja_dosen_id') ?>

    <?php // echo $form->field($model, 'gbk_1') ?>

    <?php // echo $form->field($model, 'gbk_2') ?>

    <?php // echo $form->field($model, 'aktif_start') ?>

    <?php // echo $form->field($model, 'aktif_end') ?>

    <?php // echo $form->field($model, 'deleted') ?>

    <?php // echo $form->field($model, 'deleted_at') ?>

    <?php // echo $form->field($model, 'deleted_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'temp_id_old') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
