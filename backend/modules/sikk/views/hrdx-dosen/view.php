<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\HrdxDosen */

$this->title = $model->dosen_id;
$this->params['breadcrumbs'][] = ['label' => 'Hrdx Dosens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hrdx-dosen-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->dosen_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->dosen_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'dosen_id',
            'pegawai_id',
            'nidn',
            'prodi_id',
            'golongan_kepangkatan_id',
            'jabatan_akademik_id',
            'status_ikatan_kerja_dosen_id',
            'gbk_1',
            'gbk_2',
            'aktif_start',
            'aktif_end',
            'deleted',
            'deleted_at',
            'deleted_by',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
            'temp_id_old',
        ],
    ]) ?>

</div>
