<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HrdxDosen */

$this->title = 'Create Hrdx Dosen';
$this->params['breadcrumbs'][] = ['label' => 'Hrdx Dosens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hrdx-dosen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
