<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SikkApproval */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sikk-approval-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'izin_keluar_id')->textInput() ?>

    <?= $form->field($model, 'approver_id')->textInput() ?>

    <?= $form->field($model, 'status_approval')->textInput() ?>

    <?= $form->field($model, 'alasan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deleted')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
