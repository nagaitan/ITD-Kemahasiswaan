<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/1.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?php
                    if (!Yii::$app->user->isGuest) {
                        echo Yii::$app->user->identity->user_name;
                    } else {
                        echo 'Guest';
                    }
                    ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->


        <?php
        if (!Yii::$app->user->isGuest) {

            $mus = Yii::$app->user->identity->id;

            $akunID = \common\models\SikkAkun::findOne(array('akun_id' => $mus));

            $roleID = $akunID->role_id;


            if ($roleID == 1) {
                //mahasiswa

                echo dmstr\widgets\Menu::widget(
                        [
                            'options' => ['class' => 'sidebar-menu'],
                            'items' => [
                                    ['label' => 'Home', 'url' => ['/site/index']],
                                    ['label' => 'Create Request', 'url' => ['/sikk/sikk-izin-keluar/create']],
//                                    [
//                                    'label' => 'Rencana Kerja dan Anggaran', 'visible' => \Yii::$app->user->can('menambahUsulan'),
//                                    'items' => [
//                                            ['label' => 'Daftar Program', 'url' => ['/kodeprogram/daftarprogram'], 'visible' => \Yii::$app->user->can('menambahUsulan')],
//                                            ['label' => 'Daftar Kegiatan', 'url' => ['/kodeprogram/'], 'visible' => \Yii::$app->user->can('menambahUsulan')],
//                                            ['label' => 'Daftar Rencana Kerja SKPD', 'url' => ['/detailrenjaskpd/daftarrenjaskpd'], 'visible' => \Yii::$app->user->can('menambahUsulan')],
//                                            ['label' => 'Tambah Detail Belanja Langsung', 'url' => ['/rka/'], 'visible' => \Yii::$app->user->can('menambahUsulan')],
//                                            ['label' => 'Tambah Rincian Anggaran', 'url' => ['/detailrka/'], 'visible' => \Yii::$app->user->can('menambahUsulan')],
//                                    ]
//                                ],
                            ],
                        ]
                );
            } else if ($roleID == 2) {
                //dosen pengampu
                echo dmstr\widgets\Menu::widget(
                        [
                            'options' => ['class' => 'sidebar-menu'],
                            'items' => [
                                    ['label' => 'Home', 'url' => ['/site/index']],
                                    ['label' => 'Response Dosen Pengampu', 'url' => ['/sikk/sikk-izin-keluar/']],
                            ],
                        ]
                );
            } else if ($roleID == 3) {
                //keasramaan
                echo dmstr\widgets\Menu::widget(
                        [
                            'options' => ['class' => 'sidebar-menu'],
                            'items' => [
                                    ['label' => 'Home', 'url' => ['/site/index']],
                                    ['label' => 'Response Keasramaan', 'url' => ['/sikk/sikk-izin-keluar/']],
                            ],
                        ]
                );
            } else if ($roleID == 4) {
                //dosen wali
                echo dmstr\widgets\Menu::widget(
                        [
                            'options' => ['class' => 'sidebar-menu'],
                            'items' => [
                                    ['label' => 'Home', 'url' => ['/site/index']],
                                    ['label' => 'Response Dosen Wali', 'url' => ['/sikk/sikk-izin-keluar/']],
                            ],
                        ]
                );
            } else if ($roleID == 5) {
                //satpam
                echo dmstr\widgets\Menu::widget(
                        [
                            'options' => ['class' => 'sidebar-menu'],
                            'items' => [
                                    ['label' => 'Home', 'url' => ['/site/index']],
                                    ['label' => 'Log Keluar Masuk Kampus', 'url' => ['/sikk/sikk-log/create']],
                                    ['label' => 'Daftar Izin Keluar yang disetujui', 'url' => ['/sikk/sikk-izin-keluar/index2']],
                                    ['label' => 'Daftar Izin Keluar yang ditolak', 'url' => ['/sikk/sikk-izin-keluar/index3']],
                            ],
                        ]
                );
            }
        } else {
            echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                                ['label' => 'Home', 'url' => ['/site/index']],
                        ],
                    ]
            );
        }
        ?>


    </section>

</aside>
