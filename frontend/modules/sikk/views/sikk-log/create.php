<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SikkLog */

$this->title = 'Create Sikk Log';
$this->params['breadcrumbs'][] = ['label' => 'Sikk Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sikk-log-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
