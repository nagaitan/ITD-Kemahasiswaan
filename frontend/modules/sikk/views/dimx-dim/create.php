<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DimxDim */

$this->title = 'Create Dimx Dim';
$this->params['breadcrumbs'][] = ['label' => 'Dimx Dims', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dimx-dim-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
