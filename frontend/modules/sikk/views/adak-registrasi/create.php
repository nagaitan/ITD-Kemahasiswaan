<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AdakRegistrasi */

$this->title = 'Create Adak Registrasi';
$this->params['breadcrumbs'][] = ['label' => 'Adak Registrasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adak-registrasi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
