<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\AdakRegistrasi */

$this->title = $model->registrasi_id;
$this->params['breadcrumbs'][] = ['label' => 'Adak Registrasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adak-registrasi-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->registrasi_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->registrasi_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'registrasi_id',
            'nim',
            'status_akhir_registrasi',
            'ta',
            'sem_ta',
            'sem',
            'tgl_daftar',
            'keuangan',
            'kelas',
            'id',
            'nr',
            'koa_approval',
            'koa_approval_bp',
            'kelas_id',
            'dosen_wali_id',
            'keasramaan_id',
            'dim_id',
            'deleted',
            'deleted_at',
            'deleted_by',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
