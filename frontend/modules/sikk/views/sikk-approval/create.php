<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SikkApproval */

$this->title = 'Create Sikk Approval';
$this->params['breadcrumbs'][] = ['label' => 'Sikk Approvals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sikk-approval-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
