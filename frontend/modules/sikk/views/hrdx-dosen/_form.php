<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\HrdxDosen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hrdx-dosen-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pegawai_id')->textInput() ?>

    <?= $form->field($model, 'nidn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prodi_id')->textInput() ?>

    <?= $form->field($model, 'golongan_kepangkatan_id')->textInput() ?>

    <?= $form->field($model, 'jabatan_akademik_id')->textInput() ?>

    <?= $form->field($model, 'status_ikatan_kerja_dosen_id')->textInput() ?>

    <?= $form->field($model, 'gbk_1')->textInput() ?>

    <?= $form->field($model, 'gbk_2')->textInput() ?>

    <?= $form->field($model, 'aktif_start')->textInput() ?>

    <?= $form->field($model, 'aktif_end')->textInput() ?>

    <?= $form->field($model, 'deleted')->textInput() ?>

    <?= $form->field($model, 'deleted_at')->textInput() ?>

    <?= $form->field($model, 'deleted_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'temp_id_old')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
