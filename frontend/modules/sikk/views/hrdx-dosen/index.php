<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\HrdxDosenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hrdx Dosens';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hrdx-dosen-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Hrdx Dosen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'dosen_id',
            'pegawai_id',
            'nidn',
            'prodi_id',
            'golongan_kepangkatan_id',
            // 'jabatan_akademik_id',
            // 'status_ikatan_kerja_dosen_id',
            // 'gbk_1',
            // 'gbk_2',
            // 'aktif_start',
            // 'aktif_end',
            // 'deleted',
            // 'deleted_at',
            // 'deleted_by',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',
            // 'temp_id_old',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
