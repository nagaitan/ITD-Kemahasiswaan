<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AdakKelas */

$this->title = 'Update Adak Kelas: ' . $model->kelas_id;
$this->params['breadcrumbs'][] = ['label' => 'Adak Kelas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kelas_id, 'url' => ['view', 'id' => $model->kelas_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="adak-kelas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
