<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\AdakKelas */

$this->title = $model->kelas_id;
$this->params['breadcrumbs'][] = ['label' => 'Adak Kelas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adak-kelas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->kelas_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->kelas_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kelas_id',
            'ta',
            'nama',
            'ket:ntext',
            'dosen_wali_id',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
            'deleted',
            'deleted_at',
            'deleted_by',
        ],
    ]) ?>

</div>
