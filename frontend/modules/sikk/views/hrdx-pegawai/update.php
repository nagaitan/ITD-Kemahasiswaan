<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HrdxPegawai */

$this->title = 'Update Hrdx Pegawai: ' . $model->pegawai_id;
$this->params['breadcrumbs'][] = ['label' => 'Hrdx Pegawais', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pegawai_id, 'url' => ['view', 'id' => $model->pegawai_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hrdx-pegawai-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
