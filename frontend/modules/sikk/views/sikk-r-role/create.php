<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SikkRRole */

$this->title = 'Create Sikk Rrole';
$this->params['breadcrumbs'][] = ['label' => 'Sikk Rroles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sikk-rrole-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
