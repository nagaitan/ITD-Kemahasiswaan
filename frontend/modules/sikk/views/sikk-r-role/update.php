<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SikkRRole */

$this->title = 'Update Sikk Rrole: ' . $model->role_id;
$this->params['breadcrumbs'][] = ['label' => 'Sikk Rroles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->role_id, 'url' => ['view', 'id' => $model->role_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sikk-rrole-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
